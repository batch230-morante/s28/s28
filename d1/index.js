// CRUD Operations (Create, Read, Update, Delete)
/*
    - CRUD operations are the heart of any backend application
    - Mastering the CRUD operations is essential for any developer 
    - This helps in building character and increasing exposure to logical statements
    that will help us manipulate our data
    - Mastering the CRUD operation of any language makes us a valuable developer and
    makes the work easier for us to deal with huge amounts of information
*/

// [SECTION] Inserting Documents (CREATE)
/*
    Syntax:
        - db.collectionName.insertOne({object});
*/

// Insert one (Inserts one document)
db.users.insertOne({
    firstName: "Jane",
    lastName: "Doe",
    age: 21,
    contact: {
        phone: "87654321",
        email: "janedoe@mail.com"
    },
    courses: ["CSS", "JavaScript", "Python"],
    department: "none"
})


// Inser Many (Inserts one or more documents)
/*
    - Syntax
    - db.collectionName.insertMany([{objectA}, {objectB}]);
*/

db.users.insertMany(
    [
        {
            firstName: "Stephen",
            lastName: "Hawking",
            age: 76,
            contact: {
                phone: "87000",
                email: "stephenhawking@mail.com"
            },
            courses: ["Python", "React", "PHP"],
            department: "none"
        },
        {
            firstName: "Neil",
            lastName: "Armstrong",
            age: 82,
            contact: {
                phone: "4770220",
                email: "neilarmstrong@mail.com"
            },
            courses: ["React", "Laravel", "MongoDB"],
            department: "none"
        },
    ]
)

// [SECTION] Finding Documents
/*
    - db.collectionName.find();
    - db.collectionName.find({field: value});
*/


// retrieves all documents
db.users.find();

// retrieve specific document/s
db.users.find({firstName: "Stephen"});

// multiple criteria
db.users.find({lastName: "Armstrong", age: 82});

// [SECTION] Update and Replace (UPDATE)
// Inserting something we want to update
db.users.insertOne({
    firstName: "Test",
    lastName: "Test",
    age: 0,
    contact: {
        phone: "00000000",
        email: "test@gmail.com"
    },
    courses: [],
    department: "none"
});
/*
    Syntax:
    - db.collectionName.updateOne({criteria}, {$set: {field: value}});
*/

db.users.updateOne(
    {firstName: "Test"},
    {
        $set: {
            firstName: "Bill",
            lastName: "Gates",
            age: 65,
            contact: {
                phone: "12345678",
                email: "imrich@gmail.com"
            },
            courses: ["PHP", "Laravel", "HTML"],
            department: "Operations",
            status: "active"
        }
    }
)

db.users.updateOne(
    {firstName: "Jane"},
    {
        $set:
        {
            firstName: "Jenny"
        }
    }
)

// updating multiple documents that matches field and value / criteria
db.users.updateMany(
	{department: "none"},
	{
		$set: 
		{
			department:"HR"
		}
	}
)








// [SECTION] Delete(DELETE)
/*
    db.collectionName.deleteOne({field: value});
    db.collectionName.deleteMany({field: value});
*/

// deletes one document that matches the field and value
db.users.deleteOne({
    firstName: "Jane";
})

// deletes all that match
db.users.deleteMany({})

db.users.find();

